const getAllVouchersMiddleware = (req,res,next) => {
    console.log("get All Vouchers (middleware)");
    next();
}

const getOneVoucherByIdMiddleware = (req,res,next) => {
    console.log("get One Vouchers (middleware)");
    next();
}

const postOneVoucherMiddleware = (req,res,next) => {
    console.log("post AOne Voucher (middleware)");
    next();
}

const putOneVoucherByIdMiddleware = (req,res,next) => {
    console.log("put One Voucher (middleware)");
    next();
}

const deleteOneVoucherByIdMiddleware = (req,res,next) => {
    console.log("get All Voucher (middleware)");
    next();
}

module.exports = {
    deleteOneVoucherByIdMiddleware,
    putOneVoucherByIdMiddleware,
    postOneVoucherMiddleware,
    getOneVoucherByIdMiddleware,
    getAllVouchersMiddleware
}