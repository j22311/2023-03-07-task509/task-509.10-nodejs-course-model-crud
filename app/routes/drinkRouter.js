//khai báo thư viện
const express = require("express");
const { getAllDrinks,
     insertADrink, 
     getADrinkById,
     updateADrinkById,
     deleteADrinkById} = require("../controllers/drinkcontroller");

// khai báo hoặc import drinkMiddleware 
const {
    deleteOneDrinksByIdMiddleware,
    putOneDrinksByIdMiddleware,
    postOneDrinksMiddleware,
    getOneDrinksByIdMiddleware,
    getAllDrinksMiddleware
} = require ('../middlewares/drinkMiddleware');

//tạo rounter
const drinkRouter = express.Router();

//--------dùng drinkRounter đến các đường dẫn trong postMan--------

//drinkROunter cho get All
drinkRouter.get('/drinks/',getAllDrinksMiddleware, getAllDrinks)

//drinkROunter cho get one
// drinkRouter.get('/drinks/:drinkId',getOneDrinksByIdMiddleware, (req,res) => {
//     let drinkId = req.params.drinkId;
//     res.json({
//         message: `get One drink Id= ${drinkId}`
//     });
// })
drinkRouter.get('/drinks/:drinkId',getOneDrinksByIdMiddleware,getADrinkById)

//drinkROunter cho post one
// drinkRouter.post('/drinks/',postOneDrinksMiddleware, (req,res) => {
//     let body = req.body;
//     res.json({
//         message: body
//     });
// })
drinkRouter.post('/drinks/',postOneDrinksMiddleware,insertADrink)

//drinkROunter cho delete one
// drinkRouter.delete('/drinks/:drinkId',deleteOneDrinksByIdMiddleware, (req,res) => {
//     let drinkId = req.params.drinkId;
//     res.json({
//         message: `delete One drink Id= ${drinkId}`
//     });
// })
drinkRouter.delete('/drinks/:drinkId',deleteOneDrinksByIdMiddleware,deleteADrinkById)

//drinkROunter cho update one
// drinkRouter.put('/drinks/:drinkId',putOneDrinksByIdMiddleware, (req,res) => {
//     const drinkId = req.params.drinkId;
//     const body = req.body;
//     res.json({
//         message: `update successfully ${drinkId}`,
//         data: body
//     });

// })
drinkRouter.put('/drinks/:drinkId',putOneDrinksByIdMiddleware,updateADrinkById)

// //export rounter để chạy trên fle index.js
module.exports = {
    drinkRouter
}