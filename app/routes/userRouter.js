//khai báo thư viện
const express = require("express");
const { insertAUser, getAllUser, getAUserById, updateAUser, deleteUserbyId } = require("../controllers/usercontroller");

// khai báo hoặc import drinkMiddleware 
const {
    deleteOneUserByIdMiddleware,
    putOneUserByIdMiddleware,
    postOneUserMiddleware,
    getOneUserByIdMiddleware,
    getAllUsersMiddleware
} = require ('../middlewares/userMiddleware');

//tạo rounter
const userRouter = express.Router();

//--------dùng userRouter đến các đường dẫn trong postMan--------
//userRouter cho get All
// userRouter.get('/users/',getAllUsersMiddleware, (req,res) => {
//     res.json({
//         message: 'get All users'
//     });
// })
userRouter.get('/users/',getAllUsersMiddleware, getAllUser)

// userRouter cho post one
// userRouter.post('/users/',postOneUserMiddleware, (req,res) => {
//     const body = req.body;
//     res.json({
//         message: 'create one users',
//         data: body
//     });
// })
userRouter.post('/users/',postOneUserMiddleware,insertAUser)

//userRouter cho update one
// userRouter.put('/users/:userId',putOneUserByIdMiddleware, (req,res) => {
//     const userId = req.params.userId;
//     const body = req.body
//     res.json({
//         message: `update one user id= ${userId}`,
//         data: body
//     });
// })
userRouter.put('/users/:userId',putOneUserByIdMiddleware,updateAUser)

//userRouter cho get one
// userRouter.get('/users/:userId',getOneUserByIdMiddleware, (req,res) => {
//     const userId = req.params.userId;
//     res.json({
//         message: `get one user id= ${userId}`
//     });
// })
userRouter.get('/users/:userId',getOneUserByIdMiddleware, getAUserById)

//userRouter cho delete one
// userRouter.delete('/users/:userId',deleteOneUserByIdMiddleware, (req,res) => {
//     const userId = req.params.userId;
//     res.json({
//         message: `delete one user id= ${userId}`
//     });
// })
userRouter.delete('/users/:userId',deleteOneUserByIdMiddleware, deleteUserbyId)

module.exports = {userRouter}

