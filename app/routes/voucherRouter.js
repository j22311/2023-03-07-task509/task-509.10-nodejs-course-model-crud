//khai báo thư viện
const express = require("express");
const { 
    insertAVoucher, 
    getAllVoucher,
     getAVoucherById, 
     updateAVoucherById,
     deleteAVoucherById
 } = require("../controllers/vouchercontrollers");

// khai báo hoặc import voucherMiddleware 
const {
    deleteOneVoucherByIdMiddleware,
    putOneVoucherByIdMiddleware,
    postOneVoucherMiddleware,
    getOneVoucherByIdMiddleware,
    getAllVouchersMiddleware
} = require('../middlewares/voucherMiddleware');

//tạo rounter
const voucherRouter = express.Router();

//--------dùng drinkRounter đến các đường dẫn trong postMan--------
// voucherRouter cho all Vouchers 
// voucherRouter.get('/vouchers/',getAllVouchersMiddleware, (req,res) => {
//     res.json({
//         message: 'get All vouchers'
//     });
// })
voucherRouter.get('/vouchers/',getAllVouchersMiddleware,getAllVoucher)
// voucherRouter cho get one voucher 
// voucherRouter.get('/vouchers/:voucherId',getOneVoucherByIdMiddleware, (req,res) => {
//     let voucherId = req.params.voucherId;
//     res.json({
//         message: `get One Voucher Id = ${voucherId}`
//     });
// })
voucherRouter.get('/vouchers/:voucherId',getOneVoucherByIdMiddleware,getAVoucherById)


//voucherROuter cho update one voucher
// voucherRouter.put('/vouchers/:voucherId',putOneVoucherByIdMiddleware, (req,res) => {
//     let voucherId = req.params.voucherId;
//     res.json({
//         message: `update One Voucher Id = ${voucherId}`
//     });
// })
voucherRouter.put('/vouchers/:voucherId',putOneVoucherByIdMiddleware,updateAVoucherById)

//voucherROuter cho delete one voucher
// voucherRouter.delete('/vouchers/:voucherId',deleteOneVoucherByIdMiddleware, (req,res) => {
//     let voucherId = req.params.voucherId;
//     res.json({
//         message: `delete One Voucher Id = ${voucherId}`
//     });
// })
voucherRouter.delete('/vouchers/:voucherId',deleteOneVoucherByIdMiddleware, deleteAVoucherById)
//voucherROuter cho create one voucher
// voucherRouter.post('/vouchers/',postOneVoucherMiddleware, (req,res) => {
//    const body = req.body;
//     res.json({
//         message: `create One Voucher`,
//         data: body
//     });
// })
voucherRouter.post('/vouchers/',postOneVoucherMiddleware,insertAVoucher)

//export rounter để chạy trên fle index.js
module.exports = {voucherRouter}
